#include <stdio.h>

int hours_input()
{
    int h;
    printf("Enter hours:\n");
    scanf("%d",&h);
    return h;
}

int minutes_input()
{
    int min;
    printf("Enter the minutes:\n");
    scanf("%d",&min);
    return min;
}

int conversion(int hours, int minutes)
{
    int total;
    total=(hours*60)+minutes;
    return total;
}

void output(int hours,int minutes,int total)
{
    printf("%d:%d = %d minutes\n",hours,minutes,total);
}

int main()
{
    int hours,minutes,total;

    hours=hours_input();
    minutes=minutes_input();
    total=conversion(hours,minutes);
    output(hours,minutes,total);

    return 0;
}