#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c;
    float x1,x2,d;
	printf("Enter the value of co-efficents of quadratic equation\n");
	scanf("%d%d%d",&a,&b,&c);
	d=((b*b)-(4*a*c));
	if(d<0)
	{
	   printf("The roots are imaginary\n");
	}
	x1=((-b)+sqrt(d))/(2*a);
	x2=((-b)-sqrt(d))/(2*a);
	printf("The roots of quadratic equation are %f and %f\n",x1,x2);
	return 0;
}	