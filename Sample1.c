#include <stdio.h>

int main()
{
    int x,y;

    printf("Enter the values of x and y:\n");
    scanf("%d,%d",&x,&y);

    x+=y;
    y=x-y;
    x-=y;

    printf("Numbers after swapping: %d and %d\n",x,y);
    return 0;
}